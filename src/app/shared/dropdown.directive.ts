import {Directive, HostBinding, HostListener} from "@angular/core";


@Directive({
  //attribute directive
  selector: '[appDropdown]'
})

export class DropdownDirective {
  //attach or detach 'open'
  @HostBinding('class.open') isOpen = false;

  @HostListener('click') toggleOpen() {
    this.isOpen =!this.isOpen;
  }
}
