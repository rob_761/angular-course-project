export class Ingredient{

  //automatically create properties and pass them from constructor using accessor 'public'
  constructor(public name: string, public amount: number){}
}
