import {RecipesComponent} from "./recipes/recipes.component";
import {ShoppingListComponent} from "./shopping-list/shopping-list.component";
import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {RecipeItemComponent} from "./recipes/recipe-list/recipe-item/recipe-item.component";
import {RecipeHomeComponent} from "./recipes/recipe-home/recipe-home.component";
import {RecipeDetailComponent} from "./recipes/recipe-detail/recipe-detail.component";
import {RecipeEditComponent} from "./recipes/recipe-edit/recipe-edit.component";

const appRoutes: Routes = [
  {path: '', redirectTo: '/recipes', pathMatch: 'full'}, //important to pathMatch, otherwise all paths will redirect
  { path: 'recipes', component: RecipesComponent,
  children: [
    {path: '', component: RecipeHomeComponent, pathMatch:'full'},
    {path: 'new', component: RecipeEditComponent}, //MUST be before dynamic path
    {path: ':id', component: RecipeDetailComponent},
    {path: ':id/edit', component: RecipeEditComponent},
  ]  },
  { path: 'shopping-list', component: ShoppingListComponent},
]

//transform to angular module
@NgModule({
  imports: [ RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
