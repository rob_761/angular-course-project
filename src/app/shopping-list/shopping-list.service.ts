import {Ingredient} from "../shared/ingredient-model";
import {EventEmitter} from "@angular/core";
import {Subject} from "rxjs/Subject";

//manage list of ingredients
export class ShoppingListService {

  ingredientsChanged = new Subject<Ingredient[]>();
  startedEditing = new Subject<number>();

  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Bananas', 2),
  ];

  addIngredient(ingredient: Ingredient){
    this.ingredients.push(ingredient);
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  getIngredients() {
    return this.ingredients.slice(); //return a copy
  }

  getIndgredient(index: number) {
    return this.ingredients[index];
  }

  addIngredients(ingredients: Ingredient[]) {
    this.ingredients.push(...ingredients); //es6 feature, turn array into list
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  updateIngredient(index: number, newIngredient: Ingredient) {
    this.ingredients[index] = newIngredient;
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  removeIngredient(index: number){
    this.ingredients.splice(index, 1); //remove 1 element starting at index
    this.ingredientsChanged.next(this.ingredients.slice());
  }
}
